module.exports = ({ value }) => ({ valid: typeof value === 'boolean' });
